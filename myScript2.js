//dengan class
class game{
    constructor(playerPick){
        this.playerPick=playerPick;
    }
    //penentu pilihan comp
    #compRand(){
        const compNum = Math.floor(Math.random()*3);
        return compNum;
    }
    //convert integer pilihan comp jadi choice
    #compChoose(){
        let temp = this.#compRand();
        console.log(temp);
        if(temp == 0){
            return "batu";
        }
        else if(temp == 1){
            return "kertas";
        }
        else{
            return "gunting";
        }
    }
    //highlight button/image pilihan player dan comp
    #highlight(playPick,compPick){
        let tempobtn = document.getElementsByName("playerimg");
        if(playPick=="batu"){tempobtn[0].style="background-color:gray"}
        else if(playPick=="kertas"){tempobtn[1].style="background-color:gray"}
        else if(playPick=="gunting"){tempobtn[2].style="background-color:gray"}

        let compbtn = document.getElementsByName("compbtn");
        if(compPick=="batu"){compbtn[0].style="background-color:gray"}
        else if(compPick=="kertas"){compbtn[1].style="background-color:gray"}
        else if(compPick=="gunting"){compbtn[2].style="background-color:gray"}
    }
    //disable button player setelah player memilih
    #disableButton(){
        let tempobtn = document.getElementsByName("playerbtn");
        for( let i = 0; i< tempobtn.length ; i++){

        
        tempobtn[i].disabled=true;
        }
    }
    //proses utama penentu pemenang dan menjalankan function yg dienkapsulasi didalam class game
    winner(playerChoose){
        let tempPlayer = this.playerPick;               //tampung pilihan player
        let tempComp = this.#compChoose();              //tampung pilihan komputer
        let stats= document.getElementById("status")    //tampung div id status untuk menampilkan pemenang
        this.#disableButton();                          //jalankan fungsi disable button
        this.#highlight(tempPlayer,tempComp);           //jalankan fungsi highlight button

        //console log variable pilihan player dan comp
         console.log(`pilihan player: ${tempPlayer}`);
         console.log(`pilihan Computer: ${tempComp}`);

        //kondisi draw
        if(tempPlayer==tempComp){
            stats.innerText="DRAW!";
        }
        //kondisi player pilih batu
        if(tempPlayer=="batu"){
            if(tempComp=="kertas")  {   stats.innerText="Computer Win!";}
            if(tempComp=="gunting") {   stats.innerText="Player Win!";}
        }
        if(tempPlayer=="kertas"){
            if(tempComp=="gunting") {   stats.innerText="Computer Win!";}
            if(tempComp=="batu")    {   stats.innerText="Player Win!";}
        }
        if(tempPlayer=="gunting"){
            if(tempComp=="batu")    {   stats.innerText="Computer Win!";}
            if(tempComp=="kertas")  {   stats.innerText="Player Win!";}
        }
        //console log pemenang
        console.log(stats.textContent);
    }
}
//function tombol reset, set disabled menjadi false dan highlight jadi transparent
function resetButton(){
    let tempobtn = document.getElementsByName("playerbtn");
    let playerImg = document.getElementsByName("playerimg");
    let compbtn = document.getElementsByName("compbtn");
    for( let i = 0; i< tempobtn.length ; i++){

    
    tempobtn[i].disabled=false;
    playerImg[i].style="background-color:transparent;";
    compbtn[i].style="background-color:transparent;";
    }
    let stats= document.getElementById("status");
    stats.innerText="VS";
}

function playerChoose(temp){
    let play= new game(temp);
    play.winner();
}